console.log("s29");
// it allows us to access methods and functions in easily creating our server
const express = require("express");

// we create an application using express
// app is our server
const app = express();

const port = 3000;
// middleware
app.use(express.json());
// allow ur app to read json.data
app.use(express.urlencoded({extended: true}));
// allow app to read data from Form


// [SECTION] Routes 
// if (request.url == "/login" && request.method = "POST") < using node.js

app.get("/", (req, res) => {
	// req.writehead => res.write => res.end < node.js

	res.send("Hello World");
})

// create a hello "/hello" route that will send a response 
// "hello from the /hello endpoint" upon sending a GET request.
app.get("/hello", (req,res)=>{
	res.send("hello from the /hello endpoint");
})

// Upon sending a Post request in the /hello endpoint, the server will response with tthe
// message: "hello there <firstName> <lastName>"
app.post("/hello", (req, res)=>{

	console.log(req.body);
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
})

// Create a mock database for users
// [SECTION] C R U D   Functionality
let users = [];
/*
            SCENARIO:
                We want to create a simple Users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:

                4. "/delete-user" route
                    - This endpoint will delete a user from the mock database, upon sending a "username" as a request.
                    - Before performing any actions in this endpoint, Make sure that the database is not empty and the user to be deleted exist.
                    - If the mock database is not empty, create a condition that will check if the user exist in the database and will peform the following actions:
                        - If the username is found in the database, remove the user and send a response "User <username> has been deleted.".
                        - If the user is not found in the database, send a response "User doesn't exists".
                    - if the mock database is empty, send a response "The user database is empty!"

                5. "/home" route
                    - This will send a response "Welcome to the homepage" upon accessing by the client.
        */

/*
    1. "/signup" route
                    - This will allow a client to register in our database using a username and password.
                    - Make sure that the client entered the complete information before saving it in the mock database.
                        - If the client entered a complete information, store it in our mock database and send a response of "User <username> successfully registered!".
                            Ex. User johndoe successfully registered!
                        - If the client entered a incomplete information, send a response "Please input BOTH Username and Password."

*/

app.post("/signup", (req,res)=>{
	if(req.body.userName != "" && req.body.password != "" && req.body.userName != undefined && 
		req.body.password != undefined){

		users.push(req.body)
			console.log(users);

		res.send(`User ${req.body.userName} successfully registered`);
	}
	else{
		res.send("Please input both username and password");
	}
})

/*
2. "/users" route
                    - This endpoint will be used to view all the users registered in our database.
                    [{
						"userName": "johndoe",
						"password": "john123"
                    }]
*/
app.get("/users", (req, res) => {
	res.send(users);
})

/*
 3. "/change-password" route
                    - This will allow a registered user to update his/her password.
                    - Make sure that the user is registered in the database before applying the changes.
                        - If the user's username is found in the database, change the user's password and send a response of "User <username>'s password has been updated."
                        - If the username is not found in the database, send a response of "User does not exist."

*/

app.patch("/change-password", (req,res)=>{

	// create a variable 
	let message;
	// creates a for loop that will loop through the elements of the "users" mock database
	for(i = 0; i < users.length; i++){
		// if the username provided in the postman and the username of the current object in
		// the loop is the same.
		if(users[i].userName == req.body.userName){

			users[i].password = req.body.password;

			message = `User ${req.body.userName}'s password has been updated`;

			// breaks out the loop once a user matches
			break;
	}
	else {
		message = "User does not exist";
	}
}
// once res.send isk initiated it will end the communication with the clent
	res.send(message);
})

app.get("/home", (req, res) =>{
 // res.writehead => res.write => res.end==================================
        res.send("Welcome to the homepage"); 
})

//  4. "/delete-user" route

app.delete("/delete-user", (req, res) => {
     if (users.length > 0){
     let userExist = false;
       	for(i = 0; i < users.length; i++){

  if (users[i].userName == req.body.userName) {
          res.send(`User ${users[i].userName} has been deleted`);    
          console.log(users[i].userName); 
            users.splice(i, 1);
            break;

    }
     }
   if (!userExist)
          res.send("User doesn't exists")
    }
   else {
          res.send("The user database is empty!")
    }

})


app.listen(port, () => console.log(`Server running at port ${port}`))



















